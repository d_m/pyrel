[
  { 
    "templateName": "normal",
    "numAffixes": "-1+d2M5",
    "minAffixLevel": "bad",
    "maxAffixLevel": "good",
    "itemRuleBoosts":
      {
        "minAffixLevel": ["10 + mapLevel;1", "(60 * mapLevel + 3 * mapLevel**2) / 500;1"],
        "maxAffixLevel": ["20 + mapLevel;1", "3 * mapLevel / 5;1"],
        "numAffixes": ["20 + mapLevel;1", "(60 * mapLevel + 3 * mapLevel**2) / 500;d2"],
        "magicLevel": ["5;d100"]
      },
    "artifact_ok": "True",
    "theme_ok": "True"
  },

  { 
    "templateName": "good",
    "numAffixes": "d3M5",
    "minAffixLevel": "average",
    "maxAffixLevel": "good",
    "itemRuleBoosts":
      {
        "itemLevel": ["100;5+M5"],
        "minAffixLevel": ["(20 + mapLevel) / 3;1"],
        "maxAffixLevel": ["20 + mapLevel;1", "33;1"],
        "numAffixes": ["20 + mapLevel;1", "(20 + mapLevel) / 3;d2"],
        "magicLevel": ["5;d100"]
      }
  },

  { 
    "templateName": "great",
    "numAffixes": "2+d4M5",
    "minAffixLevel": "average",
    "maxAffixLevel": "great",
    "itemRuleBoosts":
      {
        "itemLevel": ["100;10+M5"],
        "minAffixLevel": ["20 + mapLevel;1"],
        "maxAffixLevel": ["20 + mapLevel;1"],
        "numAffixes": ["20 + mapLevel;d2"],
        "magicLevel": ["5;d100"]
      }
  },

  { 
    "templateName": "orc lair",
    "numAffixes": "-1+d2",
    "minAffixLevel": "bad",
    "maxAffixLevel": "average",
    "artifact_ok": "False",
    "theme_ok": "False",
    "itemType":
      {
        "shield": "all",
        "hard armor": "all",
        "sword": "all",
        "hafted": "all",
        "launcher": "& Light Crossbow~"
      },
    "affixType":
      {
        "material": ["Stone", "Bone", "Iron", "Lead-Filled"],
        "quality": "all",
        "make": "Orcish"
      }
  },

  {
    "templateName": "kitchen sink",
    "itemLevel": "50+4d5M20",
    "itemType": {
        "soft armor": "all",
        "hard armor": ["all except", "Plate Mail~"],
        "shield": "all",
        "sword": "all",
        "hafted": "all",
        "polearm": "all",
        "launcher": "all" },
    "affixes": ["Brutal", "of Nightbane"],
    "magicLevel": 40,
    "numAffixes": "1+2d3M4",
    "minAffixLevel": "average",
    "maxAffixLevel": "great",
    "affixType": {
        "make": ["all except", "Orcish"],
        "material": "all",
        "quality": "all",
        "arcane": "all",
        "elemental": "all" },
    "affixLimits": {
        "levelsMin": { "average": 1, "good": 1 },
        "levelsMax": { "average": 2 },
        "typesMin": { "make": 1, "material": 1 },
        "typesMax": { "material": 2 } },
    "itemRuleBoosts": {
        "itemLevel": ["50 + mapLevel;10+M5"],
        "minAffixLevel": ["20 + mapLevel;1"],
        "maxAffixLevel": ["20 + mapLevel;1"],
        "numAffixes": ["20 + mapLevel;d2"],
        "magicLevel": ["5;d60"] }
  }
]
