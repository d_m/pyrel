## Cells are simply Containers that have a fixed position in the GameMap.

import container

class Cell(container.Container):
    ## \param pos (X, Y) tuple of our position.
    def __init__(self, pos):
        container.Container.__init__(self, key = pos)
        self.pos = pos
        ## Function to use for figuring out how to draw our contents; this
        # will be set by the display layer later. It gets called whenever
        # things enter or leave the Cell.
        # It must accept three arguments: ourselves, the item in question (or
        # None to initialize an "empty" cell), and
        # a boolean indicating if the item is entering or leaving.
        self.displayFunc = None


    ## Get a new display function, and act like we're adding all our items
    # with it.
    def setDisplayFunc(self, func):
        self.displayFunc = func
        self.displayData = dict()
        self.displayFunc(self, None, None)
        for item in self:
            self.displayFunc(self, item, True)


    ## Passthrough, but call our displayFunc.
    def subscribe(self, member):
        container.Container.subscribe(self, member)
        if self.displayFunc is not None:
            self.displayFunc(self, member, True)


    ## Passthrough, but call our displayFunc.
    def unsubscribe(self, member):
        container.Container.unsubscribe(self, member)
        if self.displayFunc is not None:
            self.displayFunc(self, member, False)


