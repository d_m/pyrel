## Various utility functions for interacting with data records.

import collections
import json
import sys
import traceback

## Load the contents of the provided file path and generate a set of 
# class instances of the specified type; return a list of created instances.
# All extra parameters are passed to the constructor of the class alongside
# the loaded record.
def loadRecords(filePath, classType, *args, **kwargs):
    filehandle = open(filePath, 'r')
    try:
        records = json.load(filehandle)
    except Exception, e:
        print "Data file %s is not valid JSON: %s" % (filePath, e)
        raise e
    filehandle.close()
    result = []
    for i, record in enumerate(records):
        try:
            result.append(classType(record, *args, **kwargs))
        except Exception, e:
            # Figure out roughly where in the file we were when we failed. 
            # Tricky, since we loaded the entire file correctly, but one of 
            # the records in it was faulty in a JSON-compatible way.
            # For now, assuming that any line that begins with a '{' is a 
            # new record.
            filehandle = open(filePath, 'r')
            lineNum = 0
            curIndex = 0
            didFindRecord = False
            for j, line in enumerate(filehandle):
                lineNum += 1
                if line[0] == '{':
                    curIndex += 1
                    if curIndex == i:
                        print "Failed to load record %d starting on line %d from data file %s:" % (i, lineNum, filePath)
                        didFindRecord = True
                        break
            if not didFindRecord:
                print "Failed to load record %d from data file %s:" % (i, filePath)
            traceback.print_exc()
            sys.exit()
    return result


## Copy values from one dict to another -- generally used to apply a template
# to a record.
def applyValues(sourceDict, targetDict):
    for key, value in sourceDict.iteritems():
        if type(value) == list:
            if key in targetDict and type(targetDict[key]) is not list:
                # Create a new list in the target.
                targetDict[key] = [targetDict[key]]
            # Extend the list.
            # \todo This seems like it will miss us data in the mods list,
            # and cause redundancies in the flags list.
            if key not in targetDict or targetDict[key] is None:
                targetDict[key] = []
            targetDict[key].extend(value)
        elif type(value) == dict:
            # Update the dict, recursively.
            if key not in targetDict or targetDict[key] is None:
                targetDict[key] = dict()
            applyValues(value, targetDict[key])
        else:
            # Replace the value, if it's there.
            targetDict[key] = value


## Wrapper around serializeDict that prunes out invalid keys from the given
# record.
def serializeRecord(record, fieldOrder):
    orderedValues = collections.OrderedDict()
    for key in fieldOrder:
        if key is not None and key in record:
            orderedValues[key] = record[key]
    return serializeDict(orderedValues, fieldOrder)


## Return a string representing the JSON serialization of the provided 
# OrderedDict according to the provided field ordering. This allows us 
# to combine multiple entries on the same row for visual compactness, without
# sacrificing legibility by stuffing *everything* onto the same row.
def serializeDict(orderedValues, ordering):
    result = '{'
    # We omit items that don't have any meaningful data (e.g. weapon 
    # data for a non-weapon), so we need to track if we actually want
    # to print a newline when we encounter None in FIELD_ORDER
    haveMeaningfulRow = False
    for key in ordering:
        if key is None:
            if haveMeaningfulRow:
                result += "\n"
                haveMeaningfulRow = False
        elif (key in orderedValues and 
                (orderedValues[key] or type(orderedValues[key]) is int)):
            result += '"%s": %s, ' % (key, json.dumps(orderedValues[key]))
            haveMeaningfulRow = True
    # Remove the last comma and newline, since they would cause a parse error.
    if result.endswith(", \n"):
        result = result[:-3]
    result += "}"
    return result


## Write out the provided list of records to a file at the given path.
# The records should be sortable and each must implement getSerialization().
def serializeRecords(path, records):
    records.sort()
    records = [e.getSerialization() for e in records]

    filehandle = open(path, 'w')
    filehandle.write('[\n')
    for record in records:
        # Can't have a comma after the last record.
        comma = ",\n\n"
        if record is records[-1]:
            comma = ''
        filehandle.write(record + comma)
    filehandle.write(']\n')
    filehandle.close()


