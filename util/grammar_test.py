from grammar import makePlural
from nose.exc import SkipTest

def test_makePlural_1():
    expectations = [("horse~", "horse"),
                    ("house", "house")]
    for (singular, expected_plural) in expectations:
        assert expected_plural == makePlural(singular, 1)

def test_makePlural_2():
    expectations = [("horse~", "horses"),
                    ("house~", "houses")]
    for (singular, expected_plural) in expectations:
        assert expected_plural == makePlural(singular, 2)

def test_makePlural_2_skipped():
    """Known failing test cases which we skip."""
    def skip(singular, plural):
        raise SkipTest("Skipping '%s' -> '%s'" % (singular, plural))
    expectations = [("staff", "staves"),
                    ("knife", "knives"),
                    ("vortex", "vortices"),
                    ("woman", "women"),
                    ("man", "men"),
                    ("witch", "witches"),
                    ("lady", "ladies"),
                    ("dwarf", "dwarves")]
    for (singular, plural) in expectations:
        yield skip, singular, plural
