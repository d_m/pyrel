## Load item records from object.txt and object_template.txt, and create
# ItemFactories for them.

import allocatorRules
import itemFactory
import affix
import loot
import util.record

import copy
import json
import os


## Maps (type, subtype) tuples to the factories needed to instantiate them.
ITEM_TYPE_SUBTYPE_MAP = dict()
## Instantiate an Item, rolling all necessary dice to randomize it.
def makeItem(key, itemLevel, gameMap, pos = None):
    if key not in ITEM_TYPE_SUBTYPE_MAP:
        raise RuntimeError("Invalid item key %s" % str(key))
    return ITEM_TYPE_SUBTYPE_MAP[key].makeItem(itemLevel, gameMap, pos)


## Directly access the factory with the given name.
def getFactory(type, subtype):
    if (type, subtype) not in ITEM_TYPE_SUBTYPE_MAP:
        raise RuntimeError("Invalid item name %s" % name)
    return ITEM_TYPE_SUBTYPE_MAP[(type, subtype)]


## Maps template names to the factories that can apply them.
TEMPLATE_NAME_MAP = dict()
def getItemTemplate(name):
    if name not in TEMPLATE_NAME_MAP:
        raise RuntimeError("Invalid item template: [%s]" % str(name))
    return TEMPLATE_NAME_MAP[name]


## Maps affix names to the factories that can apply them.
AFFIX_NAME_MAP = dict()
def getAffix(name):
    if name not in AFFIX_NAME_MAP:
        raise RuntimeError("Invalid item affix: [%s]" % str(name))
    return AFFIX_NAME_MAP[name]


## Maps template names to the factories that can apply them.
LOOT_TEMPLATE_MAP = dict()
def getLootTemplate(name):
    if name not in LOOT_TEMPLATE_MAP:
        raise RuntimeError("Invalid loot template: [%s]" % str(name))
    # Return a copy of the template to allow resolution of formulae
    return copy.deepcopy(LOOT_TEMPLATE_MAP[name])


## Returns affix minima and maxima for a given itemLevel, with caching
AFFIX_LIMITS = dict()
def getAffixLimits(itemLevel):
    # Check if we have already cached the results for this itemLevel
    if itemLevel not in AFFIX_LIMITS:
        AFFIX_LIMITS[itemLevel] = dict()
        AFFIX_LIMITS[itemLevel]['levelsMin'] = dict()
        AFFIX_LIMITS[itemLevel]['levelsMax'] = dict()
        AFFIX_LIMITS[itemLevel]['typesMin'] = dict()
        AFFIX_LIMITS[itemLevel]['typesMax'] = dict()
        # Iterate over all affix levels, check whether any have minima or
        # maxima at this itemLevel
        for key, level in AFFIX_LEVELS.iteritems():
            for allocation in level['allocations']:
                # Note that -1 means "no maximum"
                if (itemLevel >= allocation['minDepth'] and
                        (itemLevel <= allocation['maxDepth'] or
                        allocation['maxDepth'] is -1)):
                    # 0 is irrelevant as a minimum so we ignore it ...
                    if allocation['minNum'] > 0:
                        AFFIX_LIMITS[itemLevel]['levelsMin'][key] = allocation['minNum']
                    # ... but it's a relevant maximum, so we don't
                    if allocation['maxNum'] >= 0:
                        AFFIX_LIMITS[itemLevel]['levelsMax'][key] = allocation['minNum']
        # Now do the same for affix types
        for key, affixType in AFFIX_TYPES.iteritems():
            for allocation in affixType['allocations']:
                if (itemLevel >= allocation['minDepth'] and
                        (itemLevel <= allocation['maxDepth'] or
                        allocation['maxDepth'] is -1)):
                    if allocation['minNum'] > 0:
                        AFFIX_LIMITS[itemLevel]['typesMin'][key] = allocation['minNum']
                    if allocation['maxNum'] >= 0:
                        AFFIX_LIMITS[itemLevel]['typesMax'][key] = allocation['maxNum']
    # Return a copy so the caller can modify without affecting the cache
    return allocatorRules.AffixLimits(AFFIX_LIMITS[itemLevel])

## First load templates, so they're available when we load objects.
templates = util.record.loadRecords(os.path.join('data', 'object_template.txt'),
        itemFactory.ItemFactory)
for template in templates:
    TEMPLATE_NAME_MAP[template.record['templateName']] = template

# Now load the object records.
items = util.record.loadRecords(os.path.join('data', 'object.txt'), 
        itemFactory.ItemFactory)
for item in items:
    ITEM_TYPE_SUBTYPE_MAP[(item.type, item.subtype)] = item

# Now load the affixes.
affixes = util.record.loadRecords(os.path.join('data', 'object_affix.txt'),
        affix.Affix)
for affix in affixes:
    AFFIX_NAME_MAP[affix.record['name']] = affix

# Load the affix metadata.
AFFIX_LEVELS, AFFIX_TYPES = json.load(open(os.path.join('data', 'affix_meta.txt')))

# Load the object flag metadata.
OBJECT_FLAGS = json.load(open(os.path.join('data', 'object_flags.txt')))

# Load the loot templates.
templates = util.record.loadRecords(os.path.join('data', 'loot_template.txt'),
        loot.LootTemplate)
for template in templates:
    LOOT_TEMPLATE_MAP[template.record['templateName']] = template

#util.record.serializeRecords('object_template.txt', templates)
#util.record.serializeRecords('object.txt', items)
#util.record.serializeRecords('object_affix.txt', affixes)
