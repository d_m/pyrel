import gui.base.artists.ascii
import container
import gui.colors



## Artist that imitates an ASCII-mode drawing style.
# Provides a common UI drawing abstractions for prompts
# to display their interfaces
class AsciiArtist(gui.base.artists.ascii.AsciiArtist):
    def __init__(self, gameMap):
        super(AsciiArtist, self).__init__(gameMap)
        # position last rendered to the upper corner of the map display
        # used to determine if full screen redraw is required when map
        # display shifts
        self.lastUpperLeftCorner = (None, None)


    ## A single text character is the highest resolution
    # of text-based display
    def getBiggestCharacterDimensions(self):
        return (1,1)


    ## Draw the game view ASCII-style (i.e. one character per tile).
    # Draw changes to game map, and then overlay animations and
    # the current prompt, if any
    # \param dc drawingcontext for lower level graphics drawing
    # \param width width of map display view
    # \param height height of map display view
    # \param curPrompt current prompt to overlay on map, if any
    # \param shouldRedrawAll flag to refresh the whole screen or only dirty data
    def draw(self, dc, width, height, curPrompt, shouldRedrawAll):
        if height != self.numRows or width != self.numColumns:
            shouldRedrawAll = True
            self.numRows = height
            self.numColumns = width
        self.drawMap(dc, shouldRedrawAll)
        self.drawOverlay(dc)
        if curPrompt:
            curPrompt.draw(dc, self, self.gameMap)


    ## Draws the animation overlay
    # \param dc drawingcontext for lower level graphics drawing
    def drawOverlay(self, dc):
        if self.overlayData:
            for tile, symbol, color in self.overlayData:
                x, y = tile
                self.drawChar(dc, symbol, color, x, y)


    ## Draw a character of the specified color at the specified position.
    # \param dc drawingcontext for lower level graphics drawing
    # \param color either rgb tuple or name of color in gui.colors map
    def drawChar(self, dc, char, color, x, y):
        dc.DrawText(char, x, y, color)


    ## Display a one-line string at the provided position.
    # The color is a color name, and not a UI-native color
    # \param dc drawingcontext for lower level graphics drawing
    def writeString(self, dc, colorName, xPos, yPos, string):
        dc.DrawText( string, xPos, yPos, colorName )


    ## Draw the main game view -- a character for each tile in the map.
    # additionally updates the boundingBox denoting the area of
    # the map just displayed in the UI
    # \param dc drawingcontext for lower level graphics drawing
    # \param shouldRedrawAll flag to refresh the whole screen or only dirty data
    def drawMap(self, dc, shouldRedrawAll):
        if self.centerTile is None:
            xMin, yMin = self.getUpperLeftCorner()
        else:
            xMin = max(0, self.centerTile[0] - self.numColumns / 2)
            yMin = max(0, self.centerTile[1] - self.numRows / 2)

        xMax = min(self.gameMap.width, xMin + self.numColumns)
        yMax = min(self.gameMap.height, yMin + self.numRows)
        
        #avoid overflowing
        if (xMin + self.numColumns > self.gameMap.width):
            xMin = self.gameMap.width - self.numColumns
        if (yMin + self.numRows > self.gameMap.height):
            yMin = self.gameMap.height - self.numRows
        self.boundingBox = (xMin, xMax, yMin, yMax)

        if (self.lastUpperLeftCorner != (xMin, yMin)):
            shouldRedrawAll = True
            self.lastUpperLeftCorner = (xMin, yMin)

        iterator = self.dirtyCells
        if shouldRedrawAll:
            iterator = ((x, y)
                    for x in xrange(xMin, xMax)
                    for y in xrange(yMin, yMax))
        for x, y in iterator:
            if xMin <= x < xMax and yMin <= y < yMax:
                symbol, color = self.getDisplayData((x, y))
                self.drawChar(dc, symbol, color, x - xMin, y - yMin)
        self.dirtyCells.clear()

