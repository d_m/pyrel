## Various utility functions for doing random actions.

import random

## Return true if we roll below the provided value on a d100.
def passPercentage(percent):
    return random.randint(0, 99) < percent


## Return the result of rolling dice.
def rollDice(numDice, dieSize):
    if not (numDice and dieSize):
        return 0
    return sum([random.randint(1, dieSize) for i in xrange(numDice)])


## Return true if an event with odds of 1 in N occurs.
def oneIn(ceiling):
    return random.randint(0, ceiling) == 0
