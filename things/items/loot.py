## This file defines the LootTemplate class. Loot templates hold rules for
# item generation, such as which item types are allowed, how many affixes
# they're allowed to have, and which kinds. Please see
# http://bitbucket.org/derakon/pyrel/wiki/data_files for full documentation.

import itemLoader
import things.mixins.filter
import util.boostedDie

import random

## Class that sets rules and filters for item generation
class LootTemplate(things.mixins.filter.ItemFilter, things.mixins.filter.AffixFilter):
    ## \param record A dictionary containing our instantiation data
    def __init__(self, record):
        # Construct the filter mixins
        things.mixins.filter.ItemFilter.__init__(self)
        things.mixins.filter.AffixFilter.__init__(self)
        # The raw data record loaded from data/loot_template.txt
        self.record = record
        # The name of this template
        self.templateName = None
        # Dict of chance/amount pairs for random boosts to values
        self.itemRuleBoosts = {}
        # Dict of allowed item types/subtypes
        self.itemType = {}
        # The two levels for the item (itemLevel for generation,
        # magicLevel for properties)
        self.itemLevel = 0
        self.magicLevel = 0
        # Affix details
        self.numAffixes = 0
        self.minAffixLevel = None
        self.maxAffixLevel = None
        self.affixLimits = {}
        self.affixType = {}
        self.affixes = []
        # Permissions for higher magic tiers
        self.artifactChance = 0
        self.themeChance = 0
        # Copy values from record to ourselves.
        if record is not None:
            for key, value in record.iteritems():
                setattr(self, key, value)


    ## Resolve boostedDie values in the template, so they can be used.
    def resolveValues(self, itemLevel):
        # \todo Construct a tester for boostedDie that returns whether a
        # string is a valid boostedDie formula. Use this to iterate over all
        # members of self and avoid overwriting other strings.
        self.itemLevel = util.boostedDie.roll(self.itemLevel)
        self.magicLevel = util.boostedDie.roll(self.magicLevel)
        self.numAffixes = util.boostedDie.roll(self.numAffixes)


    ## Apply the 'itemRuleBoosts' in the template to the relevant rules. This
    # allows for some variety within a given template, e.g. so that some
    # 'good' items are better than others.
    def applyItemRuleBoosts(self, mapLevel):
        return
        # \todo remove the above line when replacing eval() with patashu's
        # calculator.py ...
        if not self.itemRuleBoosts:
            # No boosts to apply - set defaults
            self.itemLevel = mapLevel
            self.magicLevel = mapLevel
            return
        # Check for an itemLevel boost first, as this will be used by others
        if 'itemLevel' in self.itemRuleBoosts:
            for pair in self.itemRuleBoosts['itemLevel']:
                if random.randint(0,99) < eval(pair.split(';')[0]):
                    mapLevel += util.boostedDie.BoostedDie(pair.split(';')[1]).roll()
        # If magicLevel is not specified, assign its default value prior to
        # boosting
        if not self.magicLevel:
            self.magicLevel = mapLevel
        # Iterate over all itemRuleBoosts - this will boost itemLevel again
        for key, value in self.itemRuleBoosts:
            for pair in value:
                chance = eval(pair.split(';')[0])
                amount = pair.split(';')[1]
                if random.randint(0,99) < chance:
                    result = util.boostedDie.BoostedDie(amount).roll()
                    if key is 'minAffixLevel' or key is 'maxAffixLevel':
                        self.adjustAffixLevels(key, result)
                    else:
                        self.key += result
        # Assign the correct itemLevel (to avoid doubling any boost)
        self.itemLevel = mapLevel


    ## Deal with affix level adjustments, converting to and from text. We
    # assume that the lowest affix level has rank 0, but we want to avoid
    # hard-coding a highest level. 
    def adjustAffixLevels(self, which, amount):
        current = itemLoader.AFFIX_LEVELS[self.which]['rank']
        current += amount
        if current < 0:
            current = 0
        # Check whether new value is valid
        valid = False
        while not valid:
            for level in itemLoader.AFFIX_LEVELS:
                if level['rank'] == current:
                    self.which = level
                    valid = True
                    break
            if valid:
                break
            # It's not, so we try again
            current -= 1
            if current < 0:
                raise RuntimeError("Major problem with affix levels")
